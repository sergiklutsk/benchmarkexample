package benchmarkExample

// Use for run:
// go test -bench=BenchmarkMyJoin -benchmem

import (
	"log"
	"strings"
	"testing"
)

var strs = []string{"AAA", "BBB", "CCC", "DDD", "EEE"}

//var strs = make([]string, 1024)

func join(elems []string, sep string) string {
	var res string
	for i, s := range elems {
		res += s
		if i < len(elems)-1 {
			res += sep
		}
	}
	return res
}

func joinStringBuilder(elems []string, sep string) string {
	var res = strings.Builder{}
	for i, s := range elems {
		res.WriteString(s)
		if i < len(elems)-1 {
			res.WriteByte(sep[0])
		}
	}
	return res.String()
}

func BenchmarkMyJoin(b *testing.B) {
	log.Printf("BenchmarkMyJoin b.N = %v", b.N)
	for i := 0; i < b.N; i++ {
		_ = join(strs, ";")
	}
}

func BenchmarkMyJoinStringBuilder(b *testing.B) {
	log.Printf("BenchmarkMyJoin use StringBuilder b.N = %v", b.N)
	for i := 0; i < b.N; i++ {
		_ = joinStringBuilder(strs, ";")
	}
}

func BenchmarkMyJoinStringsJoin(b *testing.B) {
	//var res string

	log.Printf("BenchmarkStringsJoin use strings.Join b.N = %v", b.N)
	for i := 0; i < b.N; i++ {
		_ = strings.Join(strs, ";")
	}

	//log.Print(res)
}
